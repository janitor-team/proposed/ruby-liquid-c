ruby = RbConfig::CONFIG["RUBY_INSTALL_NAME"]
loader = Gem.find_files("rake/rake_test_loader.rb").first
tests = Dir["test/unit/**/*_test.rb"]
cmd = [ruby, "-w", "-Itest", loader] + tests + ["-v"]
puts cmd.join(" ")
system(*cmd) or exit(1)
